Welcome to hibatch's documentation!
===================================

Introduction
------------

hibatch is a tool that makes it easy to run a large number of scattering
calculations with Hibridon, either locally or through out a cluster.

hibatch is current under development and is not considered stable at this
moment.


hibatch documentation contents
------------------------------

.. toctree::
   :maxdepth: 2

   changelog

   tutorial
   input
   arguments

   xsec

   wizard


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
