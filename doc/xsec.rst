Extracting Cross sections
=========================

Integral cross sections
-----------------------

hibatch supports extracting integral cross sections from either ``.ics`` files
or ``.xxsc`` (hibatch changes the prefix to ``.xsc``) files. The ``.ics`` file
will be generated automatically by Hibridon if ``PRINTC=.TRUE``, while the
``.xxsc`` file can only be generated with an explicit ``INTCRS`` command in
Hibridon.

``.ics`` files contains integral cross sections with better precision and
these files are faster to process. However, for some basis type (e.g.,
symmetric tops with structureless atoms), the data can be incorrect. To
extract integral cross sections from ``.ics`` files, use::

   hibatch -i TRANSITIONS

To extract integral cross sections from ``.xxsc`` files, use::

   hibatch -I TRANSITIONS

``TRANSITIONS`` can either be a file, or a valid python list or list
comprehension that can be expanded to::

   [((j1_initial, i1_initial), (j1_final, i1_final)),
    ((j2_initial, i2_initial), (j2_final, i2_final)),
    ...]

Note that the ``TRANSITIONS`` should be quoted while issuing the arguments in
the shell so that it will not be expanded by the shell.  For example::

   hibatch -i "[((0, 0), (0, 1)), ((1, 2), (3, 4))]"

or::

   hibatch -i "[((0, 0), (j, i)) for j in range(3) for i in [0, 3, 6]]"

If using a file as the list of transitions, replace ``TRANSITIONS`` with the
file name.  The list-of-transions file can have free-style, but the four
numbers of transitions ``j_initial, i_initial, j_final, i_final`` should
appear in a single line and in the order specified above.  For example, the
following lines are all considerd valid transitions::

   1, 100 to 8, 0
   from j=1, i=100 to j=9, i=-9
   ((1, 100), (9, -6))
   initial level: 1 100; final level: 10 9
   1x100_10x6
   1>100>>>11>-9
   1 100 12 12

Actually, hibatch reads a line and find all instances that match the following
regular expression::

   [0-9-]+

and try to convert the matching substring to integers. If exact four integers
are found, the line is considered as a transition.

If a line starts with the ``#`` sign, it is considered as a comment line.

The extracted integral cross sections will be saved to an Microsoft Excel
flavored CSV file ``hibatch-ics.csv``.  The file contains both parameters in
``hibatch.csv`` and integral cross sections.  If a transition is not found in
a job, it will be replaced with zero.  If the reqested ``.ics`` or ``.xsc``
file does not exist, the integral cross sections will appear as ``N/A``.
