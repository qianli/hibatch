Tutorials to hibatch 
====================

This document is ment to give an overview of the common tasks that can be
performed using hibatch.


System requirements
-------------------
hibatch is written in Python 2. Besides the system requirements of hibridon,
a python intepretator (>=2.7) is required. The up-to-date version intepretator
can be obtained from the `official website <http://www.python.org/download/>`_
of python. The intepretator should be made accessible with::

    /usr/bin/env python2

