=======================
 What's new in hibatch
=======================

Changes for 0.1-alpha2
----------------------
- hibatch will look for similar Hibridon executable files when the
  file specified in ``hibatch.cfg`` is not found.  If no similar
  executable file can be found, hibatch exits with an error.

- A new argument, ``-B`` is added for non-interactive batch execution.
  This is **highly recommended** for the execution line in
  ``hibatch.sh`` instead of the original ``-b`` argument.

- All text files read by hibatch can be Unix files, Mac files or
  Windows files.

- All strings in the header line of ``hibatch.csv`` will be striped
  and converted to uppercase automatically.

- Standard output from Hibridon, instead of the ``Outpt`` file, will
  be collected as the output.

- Output from hibatch is slightly modified.


Proposed changes for 0.1-beta1
------------------------------
- Merge input file ``hibatch.sh`` to ``hibatch.cfg``.


