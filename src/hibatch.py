#!/usr/bin/env python2

"""Batch process scattering calculation tasks with Hibridon."""

# Author: Qianli Ma
# Last revision: 15-Mar-2012
__version__ = "0.1-alpha3"


import os
import os.path
import subprocess
import sys
import shutil
import tempfile
import csv
import argparse
import re
import time
import datetime


# Global path definations:
SELF_DIR = sys.path[0] + '/'
SELF_NAME = os.path.basename(sys.argv[0])
SELF_PATH = SELF_DIR + SELF_NAME
WORK_DIR = os.getcwd() + '/'


class BatchProject:
    """Class that contains the information to run all jobs."""

    def read_cfg(self, is_print=False):
        """Read project configuration file."""
        self.data_file = []
        for line in open(WORK_DIR + "hibatch.cfg", 'rU'):
            line = line.strip()
            if line.startswith("JobName="):
                self.job_name = line[8:]
            elif line.startswith("Prefix="):
                self.prefix = line[7:]
            elif line.startswith("JobType="):
                self.job_type = line[8:]
            elif line.startswith("Executable="):
                self.executable = os.path.expanduser(line[11:])
            elif line.startswith("QsubCommand="):
                self.qsub_command = line[12:]
            elif line.startswith("DataFile="):
	        self.data_file.append(line[9:].strip())
        if is_print:
            print "Job Name:", self.job_name
            print "Prefix:", self.prefix
            print "Job Type:", self.job_type
            print "Binary:", self.executable
            print "Qsub Command:", self.qsub_command
        return

    def rewrite_cfg(self):
        """Rewrite project configuration file with updated executable."""
        f = open(WORK_DIR + "hibatch.cfg.tmp", 'w')
        for line in open(WORK_DIR + "hibatch.cfg", 'rU'):
            line = line.strip()
            if line.startswith("Executable="):
                print >>f, "Executable={}".format(self.executable)
            else:
                print >>f, line
        f.close()
        os.rename(WORK_DIR + "hibatch.cfg.tmp", WORK_DIR + "hibatch.cfg")
        return

    def read_csv(self, is_print=False):
        """Read the parameter table hibatch.csv."""
        self.parameters = []
        self.id_list = []
        csv_reader = csv.reader(open(WORK_DIR + "hibatch.csv", 'rU'))
        row_id = 1
        is_header = True
        for row in csv_reader:
            if is_header:
                header = []
                for command in row:
                    header.append(command.strip(" \"\',").upper())
                is_header = False
            else:
                row_dict = {}
                for i in range(len(row)):
                    row_dict[header[i]] = row[i]
                self.parameters.append(row_dict)
                if 'ID' in row_dict:
                    self.id_list.append(int(row_dict['ID']))
                else:
                    self.id_list.append(row_id)
                    row_id += 1
        if is_print:
            print self.parameters
            print self.id_list
        return

    def read_cmd(self, is_print=False):
        """Read the command list hibatch.cmd."""
        try:
            f = open(WORK_DIR + "hibatch.cmd", 'rU')
            self.commands = f.readlines()
        finally:
            f.close()
        return

    def read_lst(self, is_print=False):
        """Read the list of files to collect after execution, hibatch.lst."""
        self.files = {}
        regex = re.compile(r"([a-zA-Z0-9.]+)=([a-zA-Z0-9.]+)")
        for line in open(WORK_DIR + "hibatch.lst", 'rU'):
            regex_match = regex.match(line.strip())
            if regex_match:
                self.files[regex_match.group(1)] = regex_match.group(2)
        if is_print:
            print self.files
        return

    def check_executable(self, is_interact=True):
        """Check if the Hibridon executable is valid."""
        if os.path.isfile(self.executable):
            return
        elif not is_interact:
            print "hibatch: error: Hibridon executable file"
            print "   ", self.executable
            print "does not exist"
            sys.exit(1)
        else:
            candidates = []
            exec_short = os.path.basename(self.executable)
            tmp_re_match = re.match(r"(\w+)_[0-9]+", exec_short)
            if tmp_re_match:
                exec_short = tmp_re_match.group(1)
            # Check $HOME/hibridon w/ QMa directory structure
            hib_dir = os.path.expanduser("~/hibridon/bin")
            for file_name in os.listdir(hib_dir):
                if file_name.find(exec_short) != -1:
                    candidates.append("{}/{}".format(hib_dir, file_name))
            # Check official Hibridon directory
            try:
                hib_dir = subprocess.check_output(["hibriddir"]).strip()
                hib_dir += "/bin/progs"
                for file_name in os.listdir(hib_dir):
                    if file_name.find(exec_short) != -1:
                        candidates.append("{}/{}".format(hib_dir, file_name))
            except OSError:
                pass
            # Ask the user to make a choice
            if len(candidates):
                print "hibatch: warning: executable not found"
                print
                print "Please choose from the following the Hibridon",
                print "executable file:"
                cand_id = 0
                for executable in candidates:
                    print "[{}] {}".format(cand_id, executable)
                    cand_id += 1
                print
                is_set = False
                while not is_set:
                    try:
                        cand_id = int(
                            input("Please choose Hibridon executable"
                                  "file from above (enter number): "))
                    except ValueError:
                        continue
                    try:
                        self.executable = candidates[cand_id]
                    except IndexError:
                        continue
                    print
                    print "You have chosen:"
                    print "   ", self.executable
                    answer = raw_input("Is that correct? (y/n) ")
                    if answer.lower().startswith("y"):
                        self.rewrite_cfg()
                        is_set = True
            else:
                print "hibatch: error: Hibridon executable file"
                print "   ", self.executable
                print "does not exist"
                sys.exit(1)
        return
    
    def __init__(self):
        """Parse a hibatchfile, and read cited files for info."""
        self.read_cfg()
        self.read_csv()
        self.read_cmd()
        self.read_lst()
        return


class SingleTask:
    """Contains info to make a single call of Hibridon."""

    def __init__(self, project, job_id):
        """Create a task class with job_id from project class."""
        self.job_id = job_id
        self.prefix = project.prefix
        self.executable = project.executable
        self.commands = project.commands
        self.files = project.files
        self.data_file = project.data_file
        try:
            job_index = project.id_list.index(job_id)
        except ValueError:
            print "Job #{} not found!".format(job_id)
            sys.exit(1)
        self.parameters = project.parameters[job_index]
        return

    def write_command_file(self, _dir):
        file_name = _dir + "/hibatch.com"
        try:
            f = open(file_name, 'w')
            print >>f, "inp=Hibatch.inp"
            for hib_cmd in self.parameters:
                if hib_cmd != 'ID':
                    print >>f, "{}={}".format(hib_cmd,
                        self.parameters[hib_cmd])
            print >>f, "SAVE=Hibatch.sav"
            print >>f, "run"
            for hib_cmd in self.commands:
                print >>f, hib_cmd
        finally:
            f.close()
        return

    def copy_input_file(self, _dir):
        """This function now copies data file as well."""
        os.makedirs(_dir + "/potdata")
        try:
            hib_data_dir = subprocess.check_output(["hibriddir"]).strip()
            hib_data_dir += "/bin/progs/potdata/"
        except OSError:
            hib_data_dir = WORK_DIR
        for data_file in self.data_file:
            shutil.copy(hib_data_dir + data_file, _dir + "/potdata/" + data_file)
        target_file_name = _dir + "/Hibatch.inp"
        shutil.copy(WORK_DIR + "hibatch.inp", target_file_name)
        return

    def gather_results(self, _dir):
        target_format = WORK_DIR + self.prefix + "-{:d}{}"
        # Collect input and output
        shutil.copy(_dir + "/Hibatch.sav",
            target_format.format(self.job_id, ".inp"))
        shutil.copy(_dir + "/hibatch_output",
            target_format.format(self.job_id, ".out"))
        for source_file in self.files:
            shutil.copy(_dir + "/" + source_file,
                target_format.format(self.job_id, self.files[source_file]))
        # Removing the temporary directory
        shutil.rmtree(_dir)
        return

    def execute_local(self, on_screen=False, print_info=True):
        """Execute Hibridon locally with the temporary command file."""

        # Running the task in a temporary directory
        _dir = tempfile.mkdtemp()
        if print_info:
            print "  Scratch directory:", _dir
            sys.stdout.flush()
        self.write_command_file(_dir)       # Preparing command file
        self.copy_input_file(_dir)      # Preparing input file
        os.chdir(_dir)
        if on_screen:
            # If set on_screen, all output from Hibridon will be directed
            # to stdout.
            os.system(self.executable + " < hibatch.com")
        else:
            # Here output from Hibridon will be saved to hibatch_output
            # and be harvested in self.gather_results().
            os.system(self.executable + " < hibatch.com > hibatch_output")
        os.chdir(SELF_DIR)
        # Gathering the results
        self.gather_results(_dir)
        return


# ----------------------------------------------------------------------

def print_job_info(job_id):
    print 
    print "Executing job #{} at {}".format(job_id, datetime.datetime.now())
    sys.stdout.flush()
    return

    
def execute_local(job_ids, print_info=True):
    project = BatchProject()
    project.check_executable(is_interact=True)
    for job_id in job_ids:
        task = SingleTask(project, job_id)
        if print_info:
            print_job_info(job_id)
        task.execute_local(print_info=print_info)
    return


def batch_local(print_info=True, is_interact=False, wait=0):
    time.sleep(wait)
    project = BatchProject()
    project.check_executable(is_interact=is_interact)
    if print_info:
        print "hibatch: start batch execution on ",
        sys.stdout.flush()
        os.system("uname -n")
    for job_id in project.id_list:
        tmp_file = "{}.{}-{:d}".format(WORK_DIR, project.prefix, job_id)
        out_file = "{}{}-{:d}.out".format(WORK_DIR, project.prefix,
            job_id)
        if not os.path.isfile(tmp_file) and not os.path.isfile(out_file):
            try:
                f = open(tmp_file, "w")
                print >>f, datetime.datetime.now()
            finally:
                f.close()
            task = SingleTask(project, job_id)
            if print_info:
                print_job_info(job_id)
            task.execute_local(print_info=print_info)
            try:
                os.unlink(tmp_file)
            except:
                pass
    return


def write_csv(output_file, data):
    reader = csv.reader(open(WORK_DIR + "hibatch.csv", 'rU'))
    writer = csv.writer(open(output_file, 'w'))
    row_id = 0
    for row in reader:
        writer.writerow(row + data[row_id])
        row_id += 1
    return


# ----------------------------------------------------------------------

class HibIntc:
    """I/O of integral cross section."""

    def read_from_ics(self, FILENAME):
        """
        Reading hibridon .ics file for the full matrix.

        FILENAME: path to the .ics file.

        """
        f = open(FILENAME, 'rU')
        # Line 1: date & time
        self.date = f.readline().strip()
        # Line 2: job name
        self.job_name = f.readline().strip()
        # Line 3: pot name
        self.pot_name = f.readline().strip()
        # Line 4: energy and ?
        # TODO: the second parameter
        try:
            self.energy = float(f.readline().split()[0])
        except IndexError:
            print FILENAME, "not successfully read."
        # Line 5, 6: parameters
        # TODO: what are they?
        for i in range(2): f.readline()
        # Line 7: number of levels and available levels
        line = f.readline().split()
        self.N_ALL = int(line[0])
        self.N = int(line[1])
        # Read the i, j of the levels        #TODO
        # TODO: when error?
        self.levels_all = []
        while (len(self.levels_all) != self.N_ALL):
            line = f.readline().split()
            for i in range(len(line) // 2):
                self.levels_all.append((int(line[2 * i]),
                                        int(line[2 * i + 1])))
        # Read the energies of the levels
        # TODO: when error?
        self.level_energies = []
        while (len(self.level_energies) != self.N_ALL):
            line = f.readline().split()
            for number in line:
                self.level_energies.append(float(number) * 219474.6)
        # Get the available energy dictionary.
        count = 0
        self.levels = {}
        for i in range(self.N_ALL):
            if self.level_energies[i] <= self.energy:
                self.levels[self.levels_all[i]] = count
                count += 1
        # TODO: what if count != len?
        # Read the big matrix!
        # TODO: when error?
        self.intcrs = []
        i = 0
        while (i != self.N):
            self.intcrs.append([])
            while (len(self.intcrs[i]) != self.N):
                line = f.readline().split()
                for number in line:
                    self.intcrs[i].append(float(number))
            i += 1
        f.close()
        return

    def __init__(self, input_file):
        self.read_from_ics(input_file)
        return

    def get_single(self, FROM=(1, 1), TO=(1, 1)):
        """
        Return a single integral cross section as a float number.

        FROM: a tuple consists of (J, I) for the initial state,
            default (1, 1).
        TO: a tuple consists of (J, I) for the final state, default
            (1, 1).

        """
        return self.intcrs[self.levels[TO]][self.levels[FROM]]

    def get(self, LIST_OF_TRANS):
        """
        Return an array of integral cross sections.

        LIST_OF_TRANS: a tuple consists of an arbitary number of tuples,
            each of which contains (J, I) of the initial and final states.

        """
        crs = []
        for trans in LIST_OF_TRANS:
            try:
                n = self.get_single(*trans)
            except:
                n = 0
            finally:
                crs.append(n)
        return crs


# ----------------------------------------------------------------------

class HibXXSC:
    def __init__(self, file_name):
        """Read a .xxsc file for all available data."""
        self.n_job = 0
        read_level = 0
        self.data = [{}]
        # Define RegEx matches: Job info line
        re_job_info = re.compile(
            r"IEN=[\s0-9]+RMU=\s+([0-9.]+)\s+E=\s+([0-9.]+)\s+"
            r"JTOT-1=\s+([0-9]+)\s+JTOT-2=\s+([0-9]+)\s+JTOT-D=\s+([0-9]+)")
        # Define RegEx matches: Level list line
        re_level_list = re.compile(
            r"([0-9]+)\s+([0-9]+)\s+([-0-9]+)\s+[-0-9.]+")
        # Scan the file
        for line in open(file_name, 'rU'):
            if read_level == 0:
                line = line.strip()
                if line.startswith("LEVEL LIST FOR INTEGRAL"):
                    read_level = 1
                    level_list = job_data['levels']
                    ics_list = job_data['ics']
                else:
                    match = re_job_info.match(line)
                    if match:
                        job_data = self.data[-1]
                        job_data['xmu'] = match.group(1)
                        job_data['energy'] = match.group(2)
                        job_data['jtot1'] = match.group(3)
                        job_data['jtot2'] = match.group(4)
                        job_data['jtotd'] = match.group(5)
                        job_data['levels'] = {}
                        job_data['ics'] = {}
            elif read_level == 1:
                line = line.strip()
                if line.startswith("CC INTEGRAL CROSS SECTIONS"):
                    read_level = 2
                else:
                    match = re_level_list.match(line)
                    if match:
                        level_list[int(match.group(1))] = (
                            int(match.group(2)), int(match.group(3)))
            elif read_level == 2:
                if line.strip().startswith("INTEGRAL CROSS SECTIONS"):
                    self.n_job += 1
                    self.data.append({})
                    read_level = 0
                elif line.startswith("    "):
                    # This is a column header
                    column_header_str = line.split()
                    column_header = []
                    for header in column_header_str:
                        column_header.append(int(header))
                else:
                    # Try a data line
                    try:
                        line_data = line.split()
                        row_header = int(line_data[0])
                        for column_index in range(1, len(line_data)):
                            transition = (
                                level_list[row_header],
                                level_list[column_header[column_index - 1]])
                            ics_list[transition] = float(
                                line_data[column_index])
                    except (ValueError, IndexError):
                        pass
        return
    
    def get(self, LIST_OF_TRANS):
        """Return an array of integral cross sections for the first job."""
        crs = []
        for trans in LIST_OF_TRANS:
            try:
                n = self.data[0]['ics'][trans]
            except KeyError:
                n = 0
            finally:
                crs.append(n)
        return crs


# ----------------------------------------------------------------------

def get_trans_from_file(file_name):
    """Read a list of transitions (i, j to i', j') from a text file."""
    # Text file can be in any style, and only lines with four numbers
    # and do not start with a sharp('#') sign will be read.
    transitions = []
    for line in open(file_name, 'rU'):
        line = line.strip()
        if line.startswith('#'):
            pass
        else:
            numbers = []
            for sub_str in re.findall(r'[0-9-]+', line):
                try:
                    numbers.append(int(sub_str))
                except ValueError:
                    pass
            if len(numbers) == 4:
                transitions.append(
                    ((numbers[0], numbers[1]), (numbers[2], numbers[3])))
    if not len(transitions):
        raise ValueError("invalid list-of-transitions file")
    return transitions

    
def extract_intcrs(
        transitions, is_print=False, method=HibIntc, file_perfix=".ics"):
    project = BatchProject()
    ics_list = []
    # Evaluate the argument as list of transitions, if fail, try find a
    # file consisting the transitions.
    try:
        trans_list = eval(transitions)
    except NameError:
        trans_list = get_trans_from_file(transitions)
    for job_id in project.id_list:
        ics_file = "{}{}-{:d}{}".format(
            WORK_DIR, project.prefix, job_id, file_perfix)
        if os.path.isfile(ics_file):
            intc = method(ics_file)
            ics_list.append(intc.get(trans_list))
        else:
            ics_list.append([r"N/A"] * len(trans_list))
    header = []
    for trans in trans_list:
        header.append(
            "{0[0][0]}({0[0][1]})->{0[1][0]}({0[1][1]})".format(trans))
    ics_list.insert(0, header)
    if is_print:
        print ics_list
    write_csv(WORK_DIR + "hibatch-ics.csv", ics_list)
    return


def submit_qsub(n_nodes, is_print=False):
    project = BatchProject()
    project.check_executable(is_interact=True)

    for i in range(n_nodes):
        script = "{}{}_tmp{}.sh".format(WORK_DIR, project.prefix, i)
        try:
            f = open(script, 'w')
            for line in open(WORK_DIR + "hibatch.sh", 'rU'):
                line = line.replace("__WORK_DIR__", WORK_DIR)
                line = line.replace(
                    "__SELF_PATH__",
                    "{} -B -w {:d}".format(SELF_PATH, i * 10))
                print >>f, line,
                if is_print:
                    print line,
        finally:
            f.close()
        command = project.qsub_command + " " + script
        if is_print:
            print command
        os.system(command)
    return


def clean_unfinished_tasks():
    project = BatchProject()
    for job_id in project.id_list:
        tmp_file = "{}.{}-{:d}".format(WORK_DIR, project.prefix, job_id)
        if os.path.isfile(tmp_file):
            os.unlink(tmp_file)
    return


def check_project_progress():
    project = BatchProject()
    # Get terminal width
    try:
        width = int(os.popen('stty size', 'r').read().split()[-1])
    except:
        width = 80
    width -= 6
    # Check the number of jobs
    n_completed = 0
    n_in_progress = 0
    for job_id in project.id_list:
        tmp_file = "{}.{}-{:d}".format(WORK_DIR, project.prefix, job_id)
        log_file = "{}{}-{:d}.out".format(WORK_DIR, project.prefix, job_id)
        if os.path.isfile(log_file):
            n_completed += 1
        elif os.path.isfile(tmp_file):
            n_in_progress += 1
    # Print the progress bar
    n_jobs = len(project.id_list)
    c_completed = n_completed * width // n_jobs
    c_in_progress = n_in_progress * width // n_jobs
    progress_bar = "[" + "#" * c_completed + "=" * c_in_progress + \
        "-" * (width - c_completed - c_in_progress) + "]" + \
        "{:3d}%".format(n_completed * 100 // n_jobs)
    print progress_bar
    print "warning: the progress bar is based on the number of jobs."
    return


def main():
    """Caller function, parse arguments."""
    parser = argparse.ArgumentParser(
        description="Batch processing Hibridon jobs.", prog="hibatch")

    parser.add_argument('-e', '--execute-local', action='store', nargs='*',
        type=int, help="execute selected jobs locally",
        metavar="JOB_ID")
    parser.add_argument(
        '-w', '--wait', action='store', nargs=1,
        type=int, help="wait WAIT seconds before silent batch execution",
        metavar="WAIT")
    parser.add_argument('-b', '--batch-local', action='store_true',
        help="batch execute all jobs locally")
    parser.add_argument('-B', '--batch-local-silent', action='store_true',
        help="batch execute all jobs w/o interaction")
    parser.add_argument('-s', '--submit-qsub', action='store', nargs=1, 
        type=int, help="batch execute using N_NODE nodes via qsub",
        metavar="N_NODE")
    parser.add_argument('-c', '--clean', action='store_true',
        help="clean up unfinished jobs")
    parser.add_argument('-i', '--intcrs', action='store', nargs=1,
        metavar="TRANSITIONS", help="extract integral cross sections")
    parser.add_argument(
        '-I', '--intcrs-xxsc', action='store', nargs=1,
        metavar="TRANSITIONS",
        help="extract integral cross sections from xxsc files")
    parser.add_argument('-p', '--check-progress', action='store_true',
        help="check project progress")
    parser.add_argument('-v', '--version', action='version',
        version="%(prog)s " + __version__)

    args = parser.parse_args()

    # Check if arguments are valid
    action_count = 0
    args_dict = vars(args)
    for (key, value) in args_dict.items():
        if key in ["wait"]:
            continue
        if value:
            action_count += 1
    if action_count > 1:
        parser.print_usage()
        print "{}: error:".format(SELF_NAME),
        print "only one action can be specified"
        sys.exit(0)

    # Executing tasks according to arguments
    if args.wait:
        wait_secs = args.wait[0]
    else:
        wait_secs = 0
    if args.execute_local is not None:
        execute_local(args.execute_local)
    elif args.batch_local:
        batch_local(is_interact=True)
    elif args.batch_local_silent:
        batch_local(is_interact=False, wait=wait_secs)
    elif args.intcrs is not None:
        extract_intcrs(
            args.intcrs[0], method=HibIntc, file_perfix=".ics")
    elif args.intcrs_xxsc is not None:
        extract_intcrs(
            args.intcrs_xxsc[0], method=HibXXSC, file_perfix=".xsc")
    elif args.submit_qsub is not None:
        submit_qsub(args.submit_qsub[0])
    elif args.clean:
        clean_unfinished_tasks()
    elif args.check_progress:
        check_project_progress()
    else:
        parser.print_help()

    return


if __name__ == '__main__':
    main()
