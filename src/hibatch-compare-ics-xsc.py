#!/usr/bin/env python2

__version__ = "0.1-alpha2"

import argparse
import os.path

import hibatch
from hibridon.crosssection import CrossSection


def run_comparison(proj, max_diff, id1):
    file_ics = proj.prefix + "-{}.ics".format(id1)
    file_xsc = proj.prefix + "-{}.xsc".format(id1)
    if os.path.isfile(file_ics) and os.path.isfile(file_xsc):
        c1 = CrossSection(file_ics, 'ics')
        c2 = CrossSection(file_xsc, 'xsc')
        if (c1.compare_with(c2, threshold=max_diff/100.0)):
            print("hibatch-compare-ics-xsc: ics and xsc file for job {} "
                  "matches.".format(id1))
        else:
            print("**WARNING**: ics and xsc file for job {} does not "
                  "match.".format(id1))
    return


def main():
    parser = argparse.ArgumentParser(
        description="Check if ics and xssc files from Hibridon match.",
        prog="hibatch-compare-ics-xsc")

    parser.add_argument('-d', '--max-diff', action='store', nargs=1,
                        type=float, default=[0.05], metavar="max_diff",
                        help="percentage of difference tolerated, default: "
                        "%(default)s")
    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s {}".format(__version__))
    parser.add_argument('job_id', type=int, nargs='*', action='store',
                        help="Job IDs to check, if missing all completed "
                        "jobs will be checked.")

    args = parser.parse_args()

    proj = hibatch.BatchProject()
    if not args.job_id:
        id_list = proj.id_list
    else:
        id_list = args.job_id
    for id_ in id_list:
        run_comparison(proj, args.max_diff[0], id_)
    return


if __name__ == '__main__':
    main()
