#!/usr/bin/env python2

"""Wizard for hibatch project."""

import sys
import os.path
import re


# All steps of the wizard
# (dict_id, title, default_value)
STEPS = [('name', 'Project name', ''),
         ('prefix', 'File prefix', 'hibatch'),
         ('executable', 'Hibridon executable file', ''),
         ('queue', 'Deepthought - job queue', 'narrow-long'),
         ('procs', 'Deepthought - number of processers', '6'),
         ('walltime', 'Deepthought - wall time limit', '72:00:00'),
         ('command', 'Command set for Hibridon', 'intcrs'),
         ('filelist', 'Files to collect', 'ics'),
         ('csv', 'CSV file', 'Press enter/return to continue')]
N_STEP = len(STEPS)
info = {}
PROMPT = {}

# Prompt messages
PROMPT['name'] = """
  Please enter a project name below. This name is only for you to
refer to and is not used by hibatch. You will be able to specify a
prefix of output files later.

Project Name?"""

PROMPT['prefix'] = """
  Please enter a prefix for output files. All files collected by
hibatch will be named to [prefix]-[job ID][perfix]. For example, use
prefix "test" (without quotation marks) to get files like test-10.out.

File Prefix?"""

PROMPT['executable'] = """
  Please enter the path to the Hibridon excutable file you want to use
for this project. It should be something like
    ~/hib436/bin/progs/hib_arn2_151
You can also simply input a part of the pot name like
    arn2
In that case, hibatch will list possible executable files found on
your system and ask you to choose one, and rewrite hibatch.cfg
automatically.

Hibridon Executable?"""


# Known deepthought queues
QUEUE_LIST = ['debug', 'debug-wide', 'narrow-med', 'wide-short', 'narrow-long',
              'narrow-extended', 'med-extended', 'wide-med', 'serial']
PROMPT['queue'] = """
  Please choose a deepthought queue. The info of queues can be found
    at http://deepthought.umd.edu

Choosing from "narrow-med", "narrow-long", "narrow-extended" or
"serial" is recommended. If you do not intend to run this project
on deepthought, accept the default.

Deepthought Queue?"""


# Known features
FEATURE_LIST = ['prod', 'ssd']
PROMPT['procs'] = """
  Please enter the number of cores used for each job. This should be a
number between 1 and 12. Each job uses only one node. You can also
request features here, for example, use "6:ssd" to use six-cores on
ssd nodes.  Note that Intel MKL may use more cores in real practice
when they are available. For mid-size jobs (few hundred channels), 6
or 8 cores for each job is recommend.

Number of cores per job?"""


PROMPT['walltime'] = """
  Please enter the maximum wall time allowed for hibatch. Use
DD:HH:MM:SS format. DD: can be omitted.

Maximum wall time allowed?"""


# Known types of command
CMD_TYPE = {}
CMD_TYPE['intcrs'] = """INTCRS"""
CMD_TYPE['blank'] = ""

PROMPT['command'] = """
  Please choose one of the Hibridon command sets listed below:
    intcrs -- Calculate integral cross sections and exit
    blank -- No additional commands after run
Note that you can always edit hibatch.cmd for the list. If you want to
edit the command list on your own, choose blank for now.

Hibridon Command Sets?"""


# Known types of file list
FILE_LIST = {'smt': 'Job1.smt=.smt',
             'ics': 'Job1.ics=.ics',
             'xxsc': 'Job1.xxsc=.xsc',
             'dcs': 'Job1.dcs=.dcs',
             'pcs': 'Job1.pcs=.pcs',
             'flx': 'Job.flx=.flx',
             'wfu': 'Job.wfu=.wfu'}
PROMPT['filelist'] = """
  Choose from below Hibridon output file you want to collect:
    smt  ics  xxsc  dcs  pcs  flx  wfu
Standard output and an input file written right before ``run`` will be
collected automatically. Note that you can edit hibatch.lst later if the
files you want to collect is not on the list.

  Separate your list with spaces, for example, "smt ics".

Files to Collect?"""


# CSV - prompt only
PROMPT['csv'] = """
  Please edit hibatch.csv manually with spreadsheet programs. This
file includes the parameters you want to modify for different jobs.

  You will also need a hibatch.inp file to run the project. This file
is an input file for Hibridon, and anything you specified in
hibatch.csv will overwrite the settings in hibatch.inp.

"""


# File perfixes to be modified by this wizard
FILE_PERFIX = ['.cfg', '.sh', '.cmd', '.lst', '.csv']


def prompt_yes_no(prompt, default=True):
    ans = raw_input("{} [{}/{}]:".format(prompt, default and 'Y' or 'y',
                                         default and 'n' or 'N'))
    if ans.lower().startswith('y'):
        return True
    elif ans.lower().startswith('n'):
        return False
    elif not ans:
        return default
    else:
        prompt_yes_no(prompt, default)
    return


def is_project_exist():
    for perfix in FILE_PERFIX:
        if os.path.isfile("hibatch{}".format(perfix)):
            return True
    return False


def prompt_str(prompt, default=""):
    ans = raw_input("{} [{}]:".format(prompt, default))
    if not ans:
        return default
    else:
        return ans


def print_file_info():
    print "Project already exists, please check the following files:"
    print "  hibatch.cfg: main project configuration file"
    print "  hibatch.csv: parameters for each calculation"
    print "  hibatch.inp: input file for Hibridon"
    print "  hibatch.cmd: extra Hibridon commands"
    print "  hibatch.lst: list of files to be gathered after calculation"
    print "  hibatch.sh: shell script for qsub"
    sys.exit(0)
    return


def check_input(tag, input_str):
    """Check if a string is a valid input."""
    if tag == 'prefix':
        if len(input_str):
            return input_str
        else:
            return None
    elif tag == 'queue':
        s = input_str.strip().lower()
        if s in QUEUE_LIST:
            return s
        else:
            return None
    elif tag == 'procs':
        s = input_str.strip().lower()
        try:
            tmp = int(s)
            return s
        except ValueError:
            re_match = re.match("[0-9]+:([a-z]+)", s)
            if re_match and re_match.group(1) in FEATURE_LIST:
                return s
        return None
    elif tag == 'walltime':
        s = input_str.strip()
        if re.match("[0-9]+:[0-9][0-9]:[0-9][0-9]", s) \
                or re.match("[0-9]+:[0-9][0-9]:[0-9][0-9]:[0-9][0-9]", s):
            return s
        else:
            return None
    elif tag == 'command':
        s = input_str.strip().lower()
        if s in CMD_TYPE:
            return s
        else:
            return None
    elif tag == 'filelist':
        ss = input_str.split()
        if not len(ss):
            return None
        is_valid = True
        for s in ss:
            if not s in FILE_LIST:
                is_valid = False
        if is_valid:
            return input_str
        else:
            return None
    else:
        return input_str


def wizard_step(step_id):
    global info
    try:
        tag, title, default = STEPS[step_id]
    except IndexError:
        return
    print
    print
    print "------------------------------------------------------------"
    print " hibatch-wizard: step {} of {}".format(step_id + 1, N_STEP)
    print "  ", title 
    print "------------------------------------------------------------"
    is_valid = False
    while not is_valid:
        ans = prompt_str(PROMPT[tag], default=default)
        s = check_input(tag, ans)
        if s is not None:
            info[tag] = s
            is_valid = True
    return


def show_summary():
    print
    print "------------------------------------------------------------"
    print " hibatch-wizard: Summary"
    print "------------------------------------------------------------"
    print " Project name:", info['name']
    print " File prefix:", info['prefix']
    print " Hibridon executable file:", info['executable']
    print " Deepthought: {} cores, on queue {}, wall time limit {}".format(
        info['procs'], info['queue'], info['walltime'])
    print " Extra command set for hibridon:", info['command']
    print " Files to collect:", info['filelist']
    print
    return


def modify_settings():
    print
    print "------------------------------------------------------------"
    print " hibatch-wizard: All steps"
    print "------------------------------------------------------------"
    for i in range(N_STEP):
        print " [{}] {}".format(i, STEPS[i][1])
    print
    s = prompt_str("  Please enter a step you would like to go through\n"
                   "or press enter/return to go back", "")
    try:
        i = int(s)
        wizard_step(i)
    except ValueError:
        return
    return


FMT_CFG = """# hibatch.cfg generated by hibatch-wizard

[Job]
JobName={}
Prefix={}

[Execution]
Executable={}

[qsub]
QsubCommand=qsub -q {}
"""


FMT_SH = r"""#PBS -l nodes=1:ppn={}
#PBS -l walltime={}
#PBS -d __WORK_DIR__
#PBS -S /bin/bash

. ~/.profile

python2 __SELF_PATH__ -B
"""


def write_files():
    print "Writting files ...",

    # Write hibatch.cfg
    f = open("hibatch.cfg", 'w')
    print >>f, FMT_CFG.format(
        info['name'], info['prefix'], info['executable'], info['queue'])
    f.close()

    # Write hibatch.csv
    f = open("hibatch.csv", 'w')
    print >>f, "ID,ENERGY"
    f.close()

    # Write hibatch.cmd
    f = open("hibatch.cmd", 'w')
    print >>f, CMD_TYPE[info['command']]
    f.close()

    # Write hibatch.lst
    f = open("hibatch.lst", 'w')
    for s in info['filelist'].split():
        print >>f, FILE_LIST[s]
    f.close()

    # Write hibatch.sh
    f = open("hibatch.sh", 'w')
    print >>f, FMT_SH.format(info['procs'], info['walltime'])
    f.close()
    
    print "done."
    return
    

def main():
    if is_project_exist():
        print_file_info()
        sys.exit(0)
    for i in range(N_STEP):
        wizard_step(i)
    is_finish = False
    while not is_finish:
        show_summary()
        is_finish = prompt_yes_no("Are the settings listed above correct?")
        if not is_finish:
            modify_settings()
    write_files()
    print
    print "You may want to edit manually:"
    print "  hibatch.cmd: extra Hibridon commands"
    print "  hibatch.lst: list of files to be gathered after calculation"
    print "  hibatch.csv: parameters for each calculation"
    print "and create:"
    print "  hibatch.inp: input file for Hibridon"
    print
    return
    

if __name__ == "__main__":
    main()
