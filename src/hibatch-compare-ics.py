#!/usr/bin/env python2

__version__ = "0.1-alpha2"

import sys
import argparse

import hibatch
from hibridon.crosssection import CrossSection


def run_comparison(max_diff, id1, id2):
    proj = hibatch.BatchProject()
    file_f = proj.prefix + "-{}.ics"
    f1 = file_f.format(id1)
    f2 = file_f.format(id2)
    c1 = CrossSection(f1)
    c2 = CrossSection(f2)
    if (c1.compare_with(c2, threshold=max_diff/100.0)):
        print("hibatch-compare-ics: two ics files are identical.")
    return


def main():
    parser = argparse.ArgumentParser(
        description="Compare two Hibridon ics files.",
        prog="hibatch-compare-ics")

    parser.add_argument('-d', '--max-diff', action='store', nargs=1,
                        type=float, default=[1.0], metavar="max_diff",
                        help="percentage of difference tolerated, default: "
                        "%(default)s")
    parser.add_argument('-v', '--version', action='version',
                        version="%(prog)s {}".format(__version__))
    parser.add_argument('job_id_1', type=int, nargs=1, action='store',
                        help="Job ID for the first ics file")
    parser.add_argument('job_id_2', type=int, nargs=1, action='store',
                        help="Job ID for the first ics file")

    args = parser.parse_args()

    run_comparison(args.max_diff[0], args.job_id_1[0], args.job_id_2[0])

    return
    


if __name__ == '__main__':
    main()
