prefix = ~

install: src/hibatch.py src/hibatch-wizard.py src/hibatch-compare-ics.py
	cp src/hibatch.py $(prefix)/bin
	cp src/hibatch-wizard.py $(prefix)/bin
	cp src/hibatch-compare-ics.py $(prefix)/bin
	cp src/hibatch-compare-ics-xsc.py $(prefix)/bin
	chmod 755 $(prefix)/bin/hibatch.py $(prefix)/bin/hibatch-wizard.py $(prefix)/bin/hibatch-compare-ics.py $(prefix)/bin/hibatch-compare-ics-xsc.py
	-ln -f -s $(prefix)/bin/hibatch.py $(prefix)/bin/hibatch
	-ln -f -s $(prefix)/bin/hibatch-wizard.py $(prefix)/bin/hibatch-wizard
	-ln -f -s $(prefix)/bin/hibatch-compare-ics.py $(prefix)/bin/hibatch-compare-ics
	-ln -f -s $(prefix)/bin/hibatch-compare-ics-xsc.py $(prefix)/bin/hibatch-compare-ics-xsc
